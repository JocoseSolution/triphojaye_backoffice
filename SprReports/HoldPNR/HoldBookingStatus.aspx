﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HoldBookingStatus.aspx.cs" Inherits="SprReports_HoldPNR_HoldBookingStatus" MasterPageFile="~/MasterAfterLogin.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"   rel="stylesheet" />

     <style type="text/css">
        .Bold {
            font:bold;
        }
        .hide {
            display:none;
        }
    </style>

     <div class="row">
        <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Flight Hold Booking Status </h3>
                    </div>
                      <div class="panel-body">
                        <div class="row">
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Order Id</label>
                                    <asp:TextBox ID="txt_OrderId" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_Search" runat="server" Text="Check Booking Status" CssClass="button buttonBlue" OnClick="btn_Search_Click" />
                                </div>
                            </div>
                        </div>
                     </div>
                      <div class="panel-body" runat="server" id="showPNR">
                        <div class="row">
                             <div class="col-md-2">
                                <div class="form-group" style="font:bold;">
                                    <label for="exampleInputPassword1">GDS PNR: </label>
                                    <asp:label runat="server" id="GDSPNR"></asp:label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="exampleInputPassword1">AIRLINE PNR: </label>
                                 <asp:label runat="server" id="AIRLINEPNR"></asp:label>
                            </div>
                             <div class="col-md-2">
                                <label for="exampleInputPassword1">Status: </label>
                                 <asp:label runat="server" id="Status"></asp:label>
                            </div>
                            <div class="col-md-6">
                               <div style="font:bold; color:red;" id="ErrorMsg" runat="server"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" Text="Update PNR" CssClass="button buttonBlue" OnClick="btn_result_Click" />
                                </div>
                            </div>                            
                        </div>
                     </div>
                    </div>
                 </div>
             </div>
          </div>
    


                <script type="text/javascript">
                    var UrlBase = '<%=ResolveUrl("~/") %>';
                </script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>