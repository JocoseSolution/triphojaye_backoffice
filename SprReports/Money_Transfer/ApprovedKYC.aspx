﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApprovedKYC.aspx.cs" Inherits="SprReports_Money_Transfer_ApprovedKYC" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Kyc Approval</title>
    <script src="../../Scripts/jquery-1.8.2.min.js"></script>
    <link href="../../CSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/bootstrap.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap.min.js"></script>
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <script src="../../JS/JScript.js" type="text/javascript"></script>
    <style type="text/css">
        .buttonBlue {
            background: #d9534f;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 10px 24px;
            margin: .3em 0 1em 0;
            /* width: 100%; */
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            /*background: transparent;*/
            background: #d9534f;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }
    </style>
    <!--overlay-->
    <style type="text/css">
        .overlay {
            position: relative;
            top: 0PX;
            left: 0PX;
            right: 0PX;
            bottom: 0PX;
            overflow-x: hidden;
            overflow-y: hidden;
            width: auto;
            height: auto;
            background: #fff;
            opacity: 1.5;
            z-index: 1000;
            display: initial;
        }

        #backgroundPopup {
            z-index: 50;
            position: fixed;
            /*display: none;*/
            height: 100%;
            width: 100%;
            background: #0000009e;
            top: 0px;
            left: 0px;
        }
    </style>
</head>
<body style="width: 870px; padding: 25px;">
    <form id="form1" runat="server">
        <asp:Panel ID="pnlUpdateForm" runat="server">
            <div class="row">
                <div class="col-sm-6">
                    <label>Select Type</label>
                    <asp:DropDownList class="form-control" ID="ddlType" runat="server">
                        <asp:ListItem Value="0">NOT APPROVED</asp:ListItem>
                        <asp:ListItem Value="1">APPROVED</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-6">
                    <label>Select Status</label>
                    <asp:DropDownList class="form-control" ID="ddlSatus" runat="server">
                        <asp:ListItem Value="PENDING">PENDING</asp:ListItem>
                        <asp:ListItem Value="WAITING FOR APPROVAL">WAITING FOR APPROVAL</asp:ListItem>
                        <asp:ListItem Value="COMPLETED">COMPLETED</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-12">
                    <label>Remark</label>
                    <asp:TextBox ID="txtRemark" runat="server" class="form-control" Rows="3" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row form-group">
                <div class="col-sm-12">
                    <asp:Button ID="btn_sumit" runat="server" Text="Submit" CssClass="btn btn-success col-sm-4 pull-right" OnClick="btn_sumit_Click" />                  
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlUpdateMessage" runat="server"  Visible="false">
           <div class="row form-group">
                <div class="col-sm-12 text-center" style="top:120px;">
                    <asp:Label ID="lblMessage" runat="server" Font-Size="xx-large"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <%--<script>
            OnClientClick="return ValidateForm();"
            function ValidateForm() {
                var remark = $("#<%=txtRemark.ClientID%>").val();
                if (remark == "") {
                    alert("Please enter remark !");
                    $("#<%=txtRemark.ClientID%>").focus();
                    return false;
                }
            }
        </script>
        --%>
    </form>
</body>
</html>
