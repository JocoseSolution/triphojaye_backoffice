﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Money_Transfer_dmt_direct_remitter_approval : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindDMTTranctionReport();
        }
    }

    protected void BindDMTTranctionReport()
    {
        DataTable dtRecord = GetDmtTrancReportList(null, null, null, "0");
        BindListView(dtRecord);
    }

    private void BindListView(DataTable dtRecord)
    {
        if (dtRecord != null && dtRecord.Rows.Count > 0)
        {
            Session["TransRecord"] = dtRecord;
            PagingButtom.Visible = true;
        }
        else
        {
            Session["TransRecord"] = null;
            PagingButtom.Visible = false;
        }

        lstTransaction.DataSource = dtRecord;
        lstTransaction.DataBind();
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        BindListView(GetDmtTrancReportList(Request["hidtxtAgencyName"].ToString(), Request["From"].ToString(), Request["To"].ToString(), status));
    }
    protected void lstTransaction_PagePropertiesChanged(object sender, EventArgs e)
    {
        //string status = ddlstatus.SelectedValue;
        BindListView(Session["TransRecord"] as DataTable);
    }
    //protected void btn_export_Click(object sender, EventArgs e)
    //{
    //    string status = ddlstatus.SelectedValue;

    //    DataTable dtRecord = new DataTable();
    //    if (Session["TransRecord"] != null && string.IsNullOrEmpty(Request["From"].ToString()) && string.IsNullOrEmpty(Request["To"].ToString()))
    //    {
    //        dtRecord = Session["TransRecord"] as DataTable;
    //    }
    //    else
    //    {
    //        dtRecord = GetDmtTrancReportList(Request["From"].ToString(), Request["To"].ToString(), txt_Trackid.Text, txt_OrderId.Text, status);
    //    }
    //    dtRecord = GetDataorExport(dtRecord);
    //    DataSet dsExport = new DataSet();
    //    dsExport.Tables.Add(dtRecord);
    //    STDom.ExportData(dsExport, ("DMT_Trans_Report_" + DateTime.Now));
    //}

    private DataTable GetDmtTrancReportList(string agencyid = null, string fromDate = null, string toDate = null, string status = null)
    {
        DataTable dtDueReport = new DataTable();

        try
        {
            string newfromdate = string.Empty; string newtodate = string.Empty;

            if (!string.IsNullOrEmpty(fromDate))
            {
                newfromdate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                newtodate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }

            adap = new SqlDataAdapter("sp_GetInstantPayDMTDirectRemitter", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Id", null);
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(agencyid) ? agencyid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(newfromdate) ? newfromdate.Trim() : "");
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(newtodate) ? newtodate.Trim() + " 23:59:59" : null);
            adap.SelectCommand.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(status) ? status.Trim() : null);
            adap.Fill(dtDueReport);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dtDueReport;
    }
}