﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //int role_id = 1;
        string Page_url = Request.Url.AbsolutePath;
        string result = checkAuthorization();


        if (result == "Invalideuser")
        {
            Response.Redirect("Error.aspx");
        }


    }

    public string checkAuthorization()
    {
        try
        {

            int role_id = 3;
            //int role_id = Session[{"xyz"];
            string Page_url = Request.Url.AbsolutePath;

            string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            SqlCommand cmd = new SqlCommand("CheckPageAuthorization_PP",con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pageurl", Page_url);
            cmd.Parameters.AddWithValue("@Role", role_id);

            string ret = "";
            ret = cmd.ExecuteScalar().ToString();
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            return ret;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}
