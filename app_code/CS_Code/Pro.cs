﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Pro
/// </summary>
public class Pro
{
    public int U_id { get; set; }
    public string U_type { get; set; }
    public string U_name { get; set; }
    public string U_role { get; set; }
    public int Page_id { get; set; }
    public string Page_name { get; set; }
    public string Page_url { get; set; }
    public int Root_page__id { get; set; }
    public string CheckBox { get; set; }
    public int Role_id { get; set; }
    public string Role_Type { get; set; }
    
    public string user_id{ get; set; }
    public string password { get; set; }


    public string name { get; set; }
    public string email { get; set; }
    public string mobileno { get; set; }
    public Boolean status { get; set; }
    public string showstatus { get; set; }
   

}