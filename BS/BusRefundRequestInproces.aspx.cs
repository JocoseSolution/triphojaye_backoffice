﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class BS_BusRefundRequestInproces : System.Web.UI.Page
{
    Itz_Trans_Dal objItzT = new Itz_Trans_Dal();
    ITZ_Trans objIzT = new ITZ_Trans();    
    DataSet DSCancel = new DataSet();
    string UserId = "", UserType = "";     
    string lbl_Reqid = "";
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null || Session["UID"].ToString() == "")
            {
                Response.Redirect("~/Login.aspx");
            }
            UserId = Session["UID"].ToString();
            UserType = Session["User_Type"].ToString();
            if (IsPostBack != true)
            {               
                DSCancel = RefundRequestInProcess();
                GridRefundRequest.DataSource = DSCancel;
                GridRefundRequest.DataBind();
            }
        }
        catch (Exception ex)
        {
           
        }
    }
  
    
    public void MakeRefund(BS_SHARED.SHARED shared, string remark, string cancellationID)
    {

        string refundOrderID = "RFNDBUS" + shared.orderID + "_" + DateTime.Now.ToString("HHmmss");
        string rfndStatus = "RefundRequested";
        //string easyRefundID = "";
        string easyRefundID = "ERID" + shared.orderID + "_" + DateTime.Now.ToString("HHmmss");
        string easyTransCode = "";
        try
        {
            if (!string.IsNullOrEmpty(shared.agentID))
            {
                rfndStatus = "Refunded";
                string apicancelstatus = "offlinecancelled";
                BS_DAL.SharedDAL shareddal = new BS_DAL.SharedDAL();
                UpdateBuscanceldetailinprocess(rfndStatus, remark, cancellationID, apicancelstatus, easyRefundID, Session["UID"].ToString(), shared.orderID);
                shared.refundAmt = Convert.ToString(shared.addAmt);

                if (!string.IsNullOrEmpty(shared.agentID) && !string.IsNullOrEmpty(shared.refundAmt))
                {
                    shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
                    shareddal.insertLedgerDetails(shared, "Add");
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Refunded Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Booking status updated but refund is not process.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                }
                GridRefundRequest.DataBind();
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('try again');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                GridRefundRequest.DataBind();
            }
        }
        catch (Exception ex1)
        {
            clsErrorLog.LogInfo(ex1);
        }
    }


    public DataSet RefundRequestInProcess()
    {

        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BusRefundRequestInprocess";
                sqlcmd.Parameters.AddWithValue("@cmd", "GET");
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return DS;

    }
    public string UpdateBuscanceldetailinprocess(string refundstatus,string remark,string cancellationId,string apicancelstatuss,string easyrefundid,string userid,string orderid)
          {
              string i = "";
              SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
              DataSet DS = new DataSet();
        try
        {
            
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BusRefundRequestInprocess";
                sqlcmd.Parameters.AddWithValue("@cmd", "Update");
                sqlcmd.Parameters.AddWithValue("@refundstatus", refundstatus);
                sqlcmd.Parameters.AddWithValue("@remark", remark);
                sqlcmd.Parameters.AddWithValue("@cancellationId", cancellationId);
                sqlcmd.Parameters.AddWithValue("@apicancelstatus", apicancelstatuss);
                sqlcmd.Parameters.AddWithValue("@easyrefundid", easyrefundid);                      
                sqlcmd.Parameters.AddWithValue("@uid", userid);
                sqlcmd.Parameters.AddWithValue("@orderid", orderid);            
                i= sqlcmd.ExecuteScalar().ToString();
               con.Close();              
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return i;

    }

    protected void RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {                  
             if(e.CommandName == "Reject")
            {
                try
                {
                   
                    ViewState["Counter"] = int.Parse(e.CommandArgument.ToString());
                   GridRefundRequest.Columns[4].Visible = false;
                    GridRefundRequest.Columns[15].Visible = false;
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)GridRefundRequest.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)GridRefundRequest.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)GridRefundRequest.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = true;
                    txtRemark.Visible = true;
                    lnkSubmit.Visible = true;
                    gvr.BackColor = System.Drawing.Color.Yellow;
                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "lnkHides")
            {
                try
                {
                    ViewState["Counter"] = int.Parse(e.CommandArgument.ToString());
                    GridRefundRequest.Columns[4].Visible = true;
                    GridRefundRequest.Columns[15].Visible = false;                  
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)GridRefundRequest.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)GridRefundRequest.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)GridRefundRequest.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = false;
                    txtRemark.Visible = false;
                    lnkSubmit.Visible = false;
                    gvr.BackColor = System.Drawing.Color.White;

                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "submit")
            {
                try
                {   LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    TextBox txtRemark = (TextBox)GridRefundRequest.Rows[RowIndex].FindControl("txtRemark");
                     if (txtRemark.Text=="")
                    {
                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('please Provide Remark.');javascript: window.opener.location=window.opener.location.href;", true);
                       
                    }
                     else
                     {
                         int i = 0;
                         BS_SHARED.SHARED shared = new BS_SHARED.SHARED();
                         ViewState["SEATNO"] = int.Parse(e.CommandArgument.ToString());
                        // GridRefundRequest.Columns[13].Visible = false;
                         GridRefundRequest.Columns[15].Visible = false;
                         ViewState["RowIndex"] = RowIndex;                       
                         LinkButton lnkSubmit = (LinkButton)GridRefundRequest.Rows[RowIndex].FindControl("lnkSubmit");
                         LinkButton lnkHides = (LinkButton)GridRefundRequest.Rows[RowIndex].FindControl("lnkHides");
                         LinkButton lnkreject = (LinkButton)GridRefundRequest.Rows[RowIndex].FindControl("lnkreject");
                         //Label lblAgentId = (Label)GridRefundRequest.Rows[RowIndex].FindControl("lblAgentId");
                        //Label OrderID = (Label)GridRefundRequest.Rows[RowIndex].FindControl("OrderID");
                         //Label txtCancellationid = (Label)GridRefundRequest.Rows[RowIndex].FindControl("lblCancellationID");
                         // Label lblRefund_Amt = (Label)GridRefundRequest.Rows[RowIndex].FindControl("lblRefund_Amt");
                         Label counter = (Label)GridRefundRequest.Rows[RowIndex].FindControl("TID");
                         lnkHides.Visible = false;
                         txtRemark.Visible = false;
                         lnkSubmit.Visible = false;
                         gvr.BackColor = System.Drawing.Color.White;                                                              
                         i = UpdateBuscanceldetail(Convert.ToInt32(counter.Text), Session["UID"].ToString(), "B_C", "Rejected", txtRemark.Text);
                         if (i > 0)
                         {
                             ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Rejected Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                             DSCancel = RefundRequestInProcess();
                             GridRefundRequest.DataSource = DSCancel;
                             GridRefundRequest.DataBind();
                            // GridRefundRequest.Columns[13].Visible = false;
                             GridRefundRequest.Columns[15].Visible = true;
                         }
                         else
                         {
                             ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Try Again.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                             DSCancel = RefundRequestInProcess();
                             GridRefundRequest.DataSource = DSCancel;
                             GridRefundRequest.DataBind();

                         }
                     }               
                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }
            }

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void lnkHides_Click(object sender, System.EventArgs e)
    {
        try
        {           
            GridRefundRequest.Columns[4].Visible = true;
            GridRefundRequest.Columns[15].Visible = true;        
            DSCancel = RefundRequestInProcess();
            GridRefundRequest.DataSource = DSCancel;
            GridRefundRequest.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    protected void lnkupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string lnkupdate = ((LinkButton)sender).CommandArgument.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.open('BsRefundRequestUpdate.aspx?counter=" + lnkupdate + "','Print','scrollbars=yes,width=900,height=500,top=20,left=150');", true);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }


    public int UpdateBuscanceldetail(int counter, string ExecutiveID, string ReqType, string Status, string remark)
    {
        int i = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "UpdateReIssueCancleAccept_bus";
                sqlcmd.Parameters.AddWithValue("@counter", counter);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@remark", remark);
                sqlcmd.Parameters.AddWithValue("@acceptby", ExecutiveID);
                i=sqlcmd.ExecuteNonQuery();
                con.Close();

            }
        }

        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return i;
       
    }
}