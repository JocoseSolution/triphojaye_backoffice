﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Update_BookingOrder
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myCon").ConnectionString)
    Private ObjIntDetails As New IntlDetails()
    Dim objTktCopy As New clsTicketCopy
    Dim SelectDetails As New IntlDetails



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                Dim OrderId As String = Request.QueryString("OrderId")
                Dim TransTD As String = Request.QueryString("TransID")
                tdRefNo.InnerText = OrderId.ToString()
                'Getting Header Details
                BindFltHeader()

                'Getting Pax Details
                BindTravellerInformation()

                'Getting Flight Details
                BindFlightDetails()

                'Getting Fare Details
                'lblFareInformation.Text = FareDetail(OrderId, TransTD)
                FareDetail()

            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Public Sub BindFltHeader()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim dtHeader As New DataTable
            dtHeader = SelectHeaderDetail() 'SelectDetails.SelectHeaderDetail(OrderId)
            GvFlightHeader.DataSource = dtHeader
            GvFlightHeader.DataBind()

        Catch ex As Exception

        End Try
    End Sub

   

    Public Sub BindTravellerInformation()
        Try
            ' Dim adap As New SqlDataAdapter("select * from fltPaxDetails where OrderId = '" & OrderId & "' ", con)
            'Dim dtPaxDetails As New DataTable
            'adap.Fill(dtPaxDetails)
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("usp_BUSPaxDetails_lookup", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            GvTravellerInformation.DataSource = dt2
            GvTravellerInformation.DataBind()

        Catch ex As Exception

        End Try

    End Sub

    Public Function SelectHeaderDetail() As DataTable
        Dim dt2 As New DataTable()
        Try
            ' Dim adap As New SqlDataAdapter("select * from fltPaxDetails where OrderId = '" & OrderId & "' ", con)
            'Dim dtPaxDetails As New DataTable
            'adap.Fill(dtPaxDetails)
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("usp_Get_BusDetailsWithPnrMessage", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)

            adap.Fill(dt2)


        Catch ex As Exception

        End Try
        Return dt2
    End Function

    Public Sub BindFlightDetails()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim adapFlightDetails As New SqlDataAdapter("select * from TBL_RB_SEATBOOKINGDETAILS where OrderId = '" & OrderId & "' ", con)
            Dim dtFlightDetails As New DataTable
            adapFlightDetails.Fill(dtFlightDetails)
            GvFlightDetails.DataSource = dtFlightDetails
            GvFlightDetails.DataBind()
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GvFlightHeader_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvFlightHeader.RowCancelingEdit
        Try
            GvFlightHeader.EditIndex = -1
            GvFlightHeader.Columns(4).Visible = False
            BindFltHeader()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvFlightHeader_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvFlightHeader.RowEditing
        Try
            GvFlightHeader.EditIndex = e.NewEditIndex
            GvFlightHeader.Columns(4).Visible = True
            BindFltHeader()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvFlightHeader_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvFlightHeader.RowUpdating

        Try
            Dim txtRemark As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
            If txtRemark.Text = "" Then
                ShowAlertMessage("Please Enter Remark")
            Else
                Dim OrderId As String = Request.QueryString("OrderId")
                Dim txtGdsPnr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtGdsPnr"), TextBox), TextBox)
                Dim txtAirlinePnr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtAirlinePnr"), TextBox), TextBox)
                Dim txtStatus As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtStatus"), TextBox), TextBox)

                SelectDetails.UpdateFlightHeader(txtGdsPnr.Text.Trim(), txtAirlinePnr.Text.Trim(), txtStatus.Text.Trim(), OrderId)
                SelectDetails.InsertQCFlightHeader(OrderId, "FltHeader", Session("UID").ToString(), txtRemark.Text)

                lblUpdateFltHeader.Visible = True
                lblUpdateFltHeader.Text = "Updated Successfully"
                GvFlightHeader.Columns(4).Visible = False
                GvFlightHeader.EditIndex = -1
                BindFltHeader()

            End If

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GvTravellerInformation_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvTravellerInformation.RowCancelingEdit
        Try

            GvTravellerInformation.EditIndex = -1
            GvTravellerInformation.Columns(6).Visible = False
            BindTravellerInformation()


        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvTravellerInformation_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvTravellerInformation.RowEditing
        Try
            GvTravellerInformation.EditIndex = e.NewEditIndex
            GvTravellerInformation.Columns(6).Visible = True
            BindTravellerInformation()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvTravellerInformation_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvTravellerInformation.RowUpdating
        Try
            Dim txtRemark As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
            If txtRemark.Text = "" Then
                ShowAlertMessage("Please Enter Remark")
            Else

                Dim OrderId As String = Request.QueryString("OrderId")
                Dim lblPaxId As Label = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("lblPaxId"), Label), Label)
                Dim txtTitle As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtTitle"), TextBox), TextBox)
                Dim txtFname As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtFname"), TextBox), TextBox)
                Dim txtLname As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtLname"), TextBox), TextBox)
                Dim txtType As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtType"), TextBox), TextBox)
                Dim txtTktNo As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtTktNo"), TextBox), TextBox)

                SelectDetails.UpdatePaxInformation(txtTitle.Text.Trim(), txtFname.Text.Trim(), txtLname.Text.Trim(), txtType.Text.Trim(), txtTktNo.Text.Trim(), OrderId, lblPaxId.Text.Trim())
                SelectDetails.InsertQCFlightHeader(OrderId, "FltPaxDetails", Session("UID").ToString(), txtRemark.Text)
                lblUpdatePax.Visible = True
                lblUpdatePax.Text = "Updated Successfully"
                GvTravellerInformation.Columns(6).Visible = False
                GvTravellerInformation.EditIndex = -1
                BindTravellerInformation()

            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GvFlightDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvFlightDetails.RowEditing
        Try

            GvFlightDetails.EditIndex = e.NewEditIndex
            BindFlightDetails()
            GvFlightDetails.Columns(12).Visible = True


        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvFlightDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvFlightDetails.RowCancelingEdit
        Try

            GvFlightDetails.EditIndex = -1
            GvFlightDetails.Columns(12).Visible = False
            BindFlightDetails()

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GvFlightDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvFlightDetails.RowUpdating

        Try
            Dim txtRemark As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
            If txtRemark.Text = "" Then
                ShowAlertMessage("Please Enter Remark")
            Else
                Dim OrderId As String = Request.QueryString("OrderId")
                Dim lblFltId As Label = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("lblFltId"), Label), Label)
                Dim txtDepcityName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepcityName"), TextBox), TextBox)
                Dim txtDepcityCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepcityCode"), TextBox), TextBox)
                Dim txtArrcityName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrcityName"), TextBox), TextBox)
                Dim txtArrcityCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrcityCode"), TextBox), TextBox)
                Dim txtAirlineName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirlineName"), TextBox), TextBox)
                Dim txtAirlineCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirlineCode"), TextBox), TextBox)
                Dim txtFltNo As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtFltNo"), TextBox), TextBox)
                Dim txtDepDate As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepDate"), TextBox), TextBox)
                Dim txtDepTime As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepTime"), TextBox), TextBox)
                Dim txtArrTime As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrTime"), TextBox), TextBox)
                Dim txtAirCraft As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirCraft"), TextBox), TextBox)

                SelectDetails.UpdateFlightDetails(txtDepcityName.Text.Trim(), txtDepcityCode.Text.Trim(), txtArrcityName.Text.Trim(), txtArrcityCode.Text.Trim(), txtAirlineName.Text.Trim(), txtAirlineCode.Text.Trim(), txtFltNo.Text.Trim(), txtDepDate.Text.Trim(), txtDepTime.Text.Trim(), txtArrTime.Text.Trim(), txtAirCraft.Text.Trim(), OrderId, lblFltId.Text)
                SelectDetails.InsertQCFlightHeader(OrderId, "FltDetails", Session("UID").ToString(), txtRemark.Text)

                lblUpdateFlight.Visible = True
                lblUpdateFlight.Text = "Updated Successfully"
                GvFlightDetails.Columns(12).Visible = False
                GvFlightDetails.EditIndex = -1
                BindFlightDetails()


            End If



        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try


            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    'Public Function FareDetail(ByVal OrderId As String, ByVal TransTD As String) As String
    '    Dim my_table As String = ""
    '    Try

    '        Dim dtpnr As New DataTable()
    '        dtpnr = ObjIntDetails.SelectHeaderDetail(OrderId)

    '        Dim dtpax As New DataTable()
    '        dtpax = ObjIntDetails.SelectPaxDetail(OrderId, TransTD)

    '        Dim dtagentid As New DataTable()
    '        dtagentid = ObjIntDetails.SelectAgent(OrderId)

    '        Dim dtagency As New DataTable()
    '        dtagency = ObjIntDetails.SelectAgencyDetail(dtagentid.Rows(0)("AgentID").ToString())

    '        Dim dtflight As New DataTable()
    '        dtflight = ObjIntDetails.SelectFlightDetail(OrderId)

    '        Dim dtfare As New DataTable()
    '        dtfare = ObjIntDetails.SelectFareDetail(OrderId, TransTD)

    '        Dim dtadtcount As New DataTable()
    '        dtadtcount = ObjIntDetails.CountADT(OrderId)

    '        Dim dtchdcount As New DataTable()
    '        dtchdcount = ObjIntDetails.CountCHD(OrderId)

    '        Dim dtinfcount As New DataTable()
    '        dtinfcount = ObjIntDetails.CountINF(OrderId)
    '        Dim IsCorp As Boolean = HttpContext.Current.Session("IsCorp")

    '        Dim FareType As String = ""
    '        If (dtfare.Rows(0)("FareType") <> "") Then
    '            FareType = "(" & dtfare.Rows(0)("FareType") & ")"
    '        End If

    '        If TransTD = "" OrElse TransTD Is Nothing Then
    '            Dim GrandTotal As Double = 0

    '            my_table += "<tr>"
    '            my_table += "<td colspan='3' class='bld'>Fare Information " & FareType & "</td>"
    '            my_table += "</tr>"
    '            my_table += "<tr>"
    '            my_table += "<td>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-1 medium-1 small-1 columns  bld'>Pax Detail</div>"
    '            my_table += "<div class='large-1 medium-1 small-1 columns  bld'>Base Fare</div>"
    '            my_table += "<div class='large-2 medium-2 small-2 columns  bld'>Fuel Surcharge</div>"
    '            my_table += "<div class='large-1 medium-1 small-1 columns  bld'>Tax</div>"
    '            my_table += "<div class='large-1 medium-1 small-1 columns  bld'>STax</div>"
    '            my_table += "<div class='large-2 medium-2 small-2 columns  bld'>Trans Fee</div>"
    '            my_table += "<div class='large-2 medium-2 small-2 columns  bld'>Trans Charge</div>"
    '            my_table += "<div class='large-2 medium-2 small-2 columns  bld'>TOTAL</div>"
    '            my_table += "</div>"
    '            my_table += "<div class='clear1'></div>"
    '            For Each dr2 As DataRow In dtfare.Rows
    '                If dr2("PaxType").ToString() = "ADT" Then
    '                    Dim BaseFare As Double = Convert.ToDouble(dr2("BaseFare").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
    '                    Dim Fuel As Double = Convert.ToDouble(dr2("Fuel").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
    '                    Dim Tax As Double = Convert.ToDouble(dr2("Tax").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
    '                    Dim ServiceTax As Double = Convert.ToDouble(dr2("ServiceTax").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
    '                    Dim TFee As Double = Convert.ToDouble(dr2("TFee").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
    '                    Dim TCharge As Double = 0
    '                    If (IsCorp = False) Then
    '                        TCharge = Convert.ToDouble(dr2("TCharge").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())

    '                    End If
    '                    Dim Total As Double = Convert.ToDouble(dr2("Total").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
    '                    my_table += "<div class='large-12 medium-12 small-12'>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & dr2("PaxType").ToString() & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & BaseFare & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & Fuel & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns ' id='td_taxadt'>" & Tax & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & ServiceTax & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & TFee & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns ' id='td_tcadt'>" & TCharge & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & Total & "</div>"
    '                    GrandTotal += Convert.ToDouble(dr2("Total").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
    '                    my_table += "</div>"
    '                    my_table += "<div class='clear1'></div>"
    '                End If
    '                If dr2("PaxType").ToString() = "CHD" Then
    '                    Dim BaseFare As Double = Convert.ToDouble(dr2("BaseFare").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
    '                    Dim Fuel As Double = Convert.ToDouble(dr2("Fuel").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
    '                    Dim Tax As Double = Convert.ToDouble(dr2("Tax").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
    '                    Dim ServiceTax As Double = Convert.ToDouble(dr2("ServiceTax").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
    '                    Dim TFee As Double = Convert.ToDouble(dr2("TFee").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())

    '                    Dim TCharge As Double = 0
    '                    If (IsCorp = False) Then
    '                        TCharge = Convert.ToDouble(dr2("TCharge").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())

    '                    End If
    '                    Dim Total As Double = Convert.ToDouble(dr2("Total").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
    '                    my_table += "<div class='large-12 medium-12 small-12'>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & dr2("PaxType").ToString() & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & BaseFare & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & Fuel & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns ' id='td_taxchd'>" & Tax & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & ServiceTax & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & TFee & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns ' id='td_tcchd'>" & TCharge & "</div>"

    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & Total & "</div>"
    '                    GrandTotal += Convert.ToDouble(dr2("Total").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
    '                    my_table += "</div>"
    '                    my_table += "<div class='clear1'></div>"
    '                End If
    '                If dr2("PaxType").ToString() = "INF" Then
    '                    Dim BaseFare As Double = Convert.ToDouble(dr2("BaseFare").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
    '                    Dim Fuel As Double = Convert.ToDouble(dr2("Fuel").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
    '                    Dim Tax As Double = Convert.ToDouble(dr2("Tax").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
    '                    Dim ServiceTax As Double = Convert.ToDouble(dr2("ServiceTax").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
    '                    Dim TFee As Double = Convert.ToDouble(dr2("TFee").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())

    '                    Dim TCharge As Double = 0
    '                    If (IsCorp = False) Then
    '                        TCharge = Convert.ToDouble(dr2("TCharge").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())

    '                    End If
    '                    Dim Total As Double = (Convert.ToDouble(dr2("Total").ToString())) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
    '                    my_table += "<div class='large-12 medium-12 small-12'>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & dr2("PaxType").ToString() & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & BaseFare & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & Fuel & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & Tax & "</div>"
    '                    my_table += "<div class='large-1 medium-1 small-1  columns '>" & ServiceTax & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & TFee & "</div>"
    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & TCharge & "</div>"

    '                    my_table += "<div class='large-2 medium-2 small-2  columns '>" & Total & "</div>"
    '                    GrandTotal += Convert.ToDouble(dr2("Total").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
    '                    my_table += "</div>"

    '                End If
    '            Next
    '            my_table += "<div class='large-12 medium-12 small-12'>"

    '            my_table += "<div class='large-1 medium-1 small-1 large-push-8 medium-push-8 small-push-8 columns  bld blue'>GRAND TOTAL</div>"
    '            my_table += "<div class='large-1 medium-1 small-1 columns  bld' id='td_grandtot'>" & GrandTotal & "</div>"

    '            my_table += "<div>"
    '            my_table += "</div>"
    '            my_table += "<div class='clear1'></div>"
    '            'my_table += "</td>"
    '            'my_table += "</tr>"


    '            'my_table += "</table>"
    '            'my_table += "</td>"

    '            'my_table += "</tr>"
    '        Else
    '            'my_table += "<tr>"
    '            'my_table += "<td>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-12 medium-12 small-12 bld blue'>Fare Information " & FareType & "</div>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>Base Fare</div>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>" & dtfare.Rows(0)("BaseFare").ToString() & "</div>"
    '            my_table += "</div>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>Fuel Surcharge</div>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>" & dtfare.Rows(0)("Fuel").ToString() & "</div>"
    '            my_table += "</div>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>Tax</div>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns ' id='td_perpaxtax'>" & dtfare.Rows(0)("Tax").ToString() & "</div>"
    '            my_table += "</div>"

    '            Dim BFare As Double = Convert.ToDouble(dtfare.Rows(0)("BaseFare").ToString())
    '            Dim Fuel As Double = Convert.ToDouble(dtfare.Rows(0)("Fuel").ToString())
    '            Dim Tax As Double = Convert.ToDouble(dtfare.Rows(0)("Tax").ToString())
    '            Dim Total As Double = BFare + Fuel + Tax
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>TOTAL</div>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>" & Total & "</div>"
    '            my_table += "</div>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>Service Tax</div>"
    '            If (IsCorp = True) Then
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>0</div>"

    '            Else
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>" & dtfare.Rows(0)("ServiceTax").ToString() & "</div>"

    '            End If
    '            my_table += "</div>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>Transaction Fee</div>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>" & dtfare.Rows(0)("TFee").ToString() & "</div>"
    '            my_table += "</div>"
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-6 medium-6 small-6  columns '>Transaction Charge</div>"
    '            If (IsCorp = True) Then
    '                my_table += "<div class='large-6 medium-6 small-6  columns ' id='td_perpaxtc'>0</div>"
    '            Else
    '                my_table += "<div class='large-6 medium-6 small-6  columns ' id='td_perpaxtc'>" & dtfare.Rows(0)("TCharge").ToString() & "</div>"
    '            End If

    '            my_table += "</div>"


    '            Dim ResuCharge As Double, ResuServiseCharge As Double, ResuFareDiff As Double = 0
    '            If dtpnr.Rows(0)("ResuID").ToString() <> "" AndAlso dtpnr.Rows(0)("ResuID").ToString() IsNot Nothing Then

    '                ResuCharge = Convert.ToDouble(dtpnr.Rows(0)("ResuCharge").ToString())
    '                ResuServiseCharge = Convert.ToDouble(dtpnr.Rows(0)("ResuServiseCharge").ToString())
    '                ResuFareDiff = Convert.ToDouble(dtpnr.Rows(0)("ResuFareDiff").ToString())
    '                my_table += "<div class='large-12 medium-12 small-12'>"
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>Reissue Charge</div>"
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>" & ResuCharge & "</div>"
    '                my_table += "</div>"

    '                my_table += "<div class='large-12 medium-12 small-12'>"
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>Reissue Srv. Charge</div>"
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>" & ResuServiseCharge & "</div>"
    '                my_table += "</div>"

    '                my_table += "<div class='large-12 medium-12 small-12'>"
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>Reissue Fare Diff.</div>"
    '                my_table += "<div class='large-6 medium-6 small-6  columns '>" & ResuFareDiff & "</div>"
    '                my_table += "</div>"

    '            End If




    '            Dim STax As Double = Convert.ToDouble(dtfare.Rows(0)("ServiceTax").ToString())
    '            Dim TFee As Double = Convert.ToDouble(dtfare.Rows(0)("TFee").ToString())
    '            Dim TCharge As Double = Convert.ToDouble(dtfare.Rows(0)("TCharge").ToString())
    '            Dim GrandTotal As Double = 0
    '            If dtpnr.Rows(0)("ResuID").ToString() <> "" AndAlso dtpnr.Rows(0)("ResuID").ToString() IsNot Nothing Then
    '                GrandTotal = Total + STax + TFee + TCharge + ResuCharge + ResuServiseCharge + ResuFareDiff
    '            Else
    '                GrandTotal = Total + STax + TFee + TCharge
    '            End If
    '            my_table += "<div class='large-12 medium-12 small-12'>"
    '            my_table += "<div class='large-6 medium-6 small-6 columns  bld'>GRAND TOTAL</div>"
    '            my_table += "<div class='large-6 medium-6 small-6 columns  bld' id='td_perpaxgrandtot'>" & GrandTotal & "</div>"
    '            my_table += "</div>"
    '            my_table += "</div>"
    '            my_table += "<div class='clear1'></div>"
    '            'my_table += "</td>"
    '            'my_table += "</tr>"
    '        End If
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try
    '    Return my_table
    'End Function


    Public Sub FareDetail()

        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim adapFlightDetails As New SqlDataAdapter("select * from TBL_RB_SEATBOOKINGDETAILS where OrderId = '" & OrderId & "' ", con)
            Dim dtFlightDetails As New DataTable
            adapFlightDetails.Fill(dtFlightDetails)
            GvFareDetails.DataSource = dtFlightDetails
            GvFareDetails.DataBind()
        Catch ex As Exception

        End Try
    End Sub

End Class
