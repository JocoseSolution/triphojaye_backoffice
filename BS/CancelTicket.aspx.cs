﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;


public partial class BS_CancelTicket : System.Web.UI.Page
{
    BS_SHARED.SHARED shared; BS_BAL.SharedBAL sharedbal;
    List<BS_SHARED.SHARED> list; DataColumn col = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            shared = new BS_SHARED.SHARED(); string pax = "";
            sharedbal = new BS_BAL.SharedBAL();
            if ((Request.QueryString["tin"].ToString() != "" || Request.QueryString["tin"].ToString() != null) && (Request.QueryString["oid"].ToString() != "" || Request.QueryString["oid"].ToString() != null))
            {
                shared.tin = Request.QueryString["tin"].ToString().Trim();
                shared.orderID = Request.QueryString["oid"].ToString().Trim();
                
                list = sharedbal.getcancellist(shared);
                Session["list"] = list;

                if (list.Count == 0 || list[0].status == "Fail")
                {
                    divcan.Visible = false;
                    divcan_Pax.InnerHtml = "<div align='center' style='color:#203240;font-size:14px;'>The Ticket Has Already been Cancelled</div>";
                }
                else
                {
                    if (list[0].provider_name == "GS")
                    {
                        btncancel.Visible = false;
                        btnChkType.Visible = true;
                        divcan_Pax.InnerHtml = list[0].bookreq;
                    }
                    else
                    {
                        btncancel.Visible = true;
                        btnChkType.Visible = false;
                        pax += "<table cellpadding='0' cellspacing='5' border='0' class='canTbl'>";
                        pax += "<tr>";
                        pax += "<td><b>Bus Operator:</b></td>";
                        pax += "<td>" + list[0].busoperator + "</td>";
                        pax += "<td><b>Date Of Journey:</b></td>";
                        pax += "<td>" + list[0].journeyDate + "</td>";
                        pax += "</tr>";
                        pax += "<tr>";
                        pax += "<td><b>Source City:</b></td>";
                        pax += "<td>" + list[0].src + "</td>";
                        pax += "<td><b>Destination City:</b></td>";
                        pax += "<td>" + list[0].dest + "</td>";
                        pax += "</tr>";
                        pax += "<tr>";
                        pax += "<td><b>Pnr:</b></td>";
                        pax += "<td>" + list[0].pnr + "</td>";
                        pax += "<td><b>Ticket No:</b></td>";
                        pax += "<td>" + list[0].tin + "</td>";
                        pax += "</tr>";

                        pax += "<tr>";
                        pax += "<td><b>Partial Cancellation:</b></td>";
                        if (list[0].partialCanAllowed.ToUpper() == "TRUE")
                        {
                            pax += "<td>Allowed</td>";
                        }
                        else
                        {
                            pax += "<td>Not Allowed</td>";
                        }
                        pax += "<td>&nbsp;&nbsp;</td>";
                        pax += "<td>&nbsp;&nbsp;</td>";
                        pax += "</tr>";

                        pax += "<tr>";
                        pax += "<td colspan='4'><b>Note:</b> if partial cancellation is not allowed that means all tickets will cancelled at one time. </td>";
                        pax += "</tr>";


                        pax += "</table>";
                        divcan_Pax.InnerHtml = pax;
                        BindPax(list);
                        if (list[0].partialCanAllowed.ToUpper() == "FALSE")
                        {
                            foreach (RepeaterItem item in rep_paxCan.Items)
                            {
                                CheckBox chkk = (CheckBox)item.FindControl("chkChild");
                                chkk.Checked = true;
                                chkk.Enabled = false;
                            }
                        }
                    }
                }
            }

        }
    }
    private void BindPax(List<BS_SHARED.SHARED> list)
    {
        DataTable paxdt = new DataTable();
        col = new DataColumn();
        col.ColumnName = "PaxTP";
        paxdt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "PaxName";
        paxdt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "Fare";
        paxdt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "NetFare";
        paxdt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "seat";
        paxdt.Columns.Add(col);
        for (int k = 0; k <= list.Count - 1; k++)
        {
            DataRow dr = paxdt.NewRow();
            if (list[0].seat != "" && list[0].fare != "")
            {
                dr["PaxTP"] = "Passenger :" + (k + 1) + "";
                if (list[k].Passengername.IndexOf(",") >= 0)
                {
                    string[] name = list[k].Passengername.Split(',');
                    dr["PaxName"] = name[0].Trim();
                }
                else
                {
                    dr["PaxName"] = list[k].Passengername.Trim();
                }
                dr["Fare"] = list[k].fare.Trim();
                dr["seat"] = list[k].seat.Trim();
                dr["NetFare"] = Convert.ToString(list[k].taNetFare);
                paxdt.Rows.Add(dr);

            }
        }
        rep_paxCan.DataSource = paxdt;
        rep_paxCan.DataBind();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        try
        {

            string HiddenValue = HiddenField1.Value;
            string[] resp = null; string netfare = ""; string seat = ""; string fare = "";
            string originalfare = "";
            string[] cancelResponse = null;
            double busfare = 0; bool flagg = false; string[] cancelResp = new string[4];
            BS_DAL.SharedDAL shareddal = new BS_DAL.SharedDAL();
           // BS_BAL.advanceReserv objadv = new BS_BAL.advanceReserv();
           // BS_BAL.advanceReserv objadv1 = new BS_BAL.advanceReserv();
           // BS_BAL.advanceReserv oldresp = new BS_BAL.advanceReserv();
            List<BS_SHARED.SHARED> list = (List<BS_SHARED.SHARED>)Session["list"];
            List<BS_SHARED.SHARED> getlistGS = (List<BS_SHARED.SHARED>)Session["getlistGS"];
            shared = new BS_SHARED.SHARED(); sharedbal = new BS_BAL.SharedBAL();
            //if (list[0].provider_name == "GS")
            //{
            //    string cnstatus = "";
            //    list[0].bookreq = getlistGS[0].bookreq;
            //    GsrtcService objgsrtcService = new GsrtcService();
            //    string arrgs = list[0].bookreq.Trim().Split('_')[0];
            //    string arrgs1 = list[0].bookreq.Trim().Split('_')[1];
            //    string bookResOld = list[0].bookres.Trim();
            //    //cnstatus = objgsrtcService.confirmFullCancellation(arrgs, arrgs1);
            //    if (cnstatus == "success")
            //    {

            //        objadv = objgsrtcService.advanceReservObject(arrgs);
            //        objadv1 = objgsrtcService.advanceReservObject(arrgs1);

            //        string[] spltfare = objadv.seatNosToInActive.Trim().Split(',');
            //        // string[] splitnetfare = shared.netfare.Split(',');

            //        double OtherAmountCalcel = 0;
            //        OtherAmountCalcel = Math.Round(Convert.ToDouble(Convert.ToDouble(objadv1.canclReservationFee) + Convert.ToDouble(objadv1.canclAccidentReliefFund) + Convert.ToDouble(objadv1.canclBridgeFee) + Convert.ToDouble(objadv1.canclTollFee) + Convert.ToDouble(objadv1.canclUserFee) + Convert.ToDouble(objadv1.canclEntryFee) + Convert.ToDouble(objadv1.canclOtherLevies) + Convert.ToDouble(objadv1.cancelOtherDiscount) + Convert.ToDouble(objadv1.canclOtherConcessions)) / list.Count);

            //        for (int b = 0; b < list.Count; b++)
            //        {

            //            if (Convert.ToInt32(objadv.addnlAge[b]) < 12)
            //            {
            //                if (objadv1.refundPrcntOrLumpsum == "P")
            //                {
            //                    busfare = Math.Round(Convert.ToDouble((Convert.ToDouble(objadv.childFare) * Convert.ToDouble(objadv1.refundPrcntOrLumpsumValue)) / 100));
            //                    shared.refundAmt += Convert.ToString(Math.Round(Math.Round(Convert.ToDouble(objadv.childFare) + OtherAmountCalcel) - busfare)) + ",";
            //                }
            //                else
            //                {
            //                    busfare = Math.Round(Convert.ToDouble(Convert.ToInt32(objadv.childFare) + Convert.ToDouble(objadv1.refundPrcntOrLumpsumValue)));
            //                    shared.refundAmt += Convert.ToString(Math.Round(Math.Round(Convert.ToDouble(objadv.childFare) + OtherAmountCalcel) - busfare)) + ",";
            //                }
            //            }
            //            else
            //            {
            //                if (objadv1.refundPrcntOrLumpsum == "P")
            //                {
            //                    busfare = Math.Round(Convert.ToDouble((Convert.ToDouble(objadv.adultFare) * Convert.ToDouble(objadv1.refundPrcntOrLumpsumValue)) / 100));
            //                    shared.refundAmt += Convert.ToString(Math.Round(Math.Round(Convert.ToDouble(objadv.adultFare) + OtherAmountCalcel) - busfare)).Split('.')[0] + ",";
            //                }
            //                else
            //                {
            //                    busfare = Math.Round(Convert.ToDouble(Convert.ToInt32(objadv.adultFare) + Convert.ToDouble(objadv1.refundPrcntOrLumpsumValue)));
            //                    shared.refundAmt += Convert.ToString(Math.Round(Math.Round(Convert.ToDouble(objadv.adultFare) + OtherAmountCalcel) - busfare)).Split('.')[0] + ",";
                               
            //                }
            //            }
            //            shared.cancelRecharge += Convert.ToString(busfare) + ",";
            //        }
            //        shared.pnr = list[0].pnr;
            //        shared.refundAmt = shared.refundAmt.Trim().Substring(0, shared.refundAmt.Trim().Length - 1);
            //        shared.cancelRecharge = shared.cancelRecharge.Trim().Substring(0,shared.cancelRecharge.Trim().Length - 1);
            //        shared.orderID = list[0].orderID;
            //        shared.seat = objadv1.seatNosToInActive;
            //        shared.tin = list[0].tin;
            //        shared.agentID = list[0].agentID;//change here
            //        shared.AgencyName = list[0].AgencyName;
            //        shareddal.updateCanchrgandRefund(shared);
            //        shared.refundAmt = objadv1.netRefundAmount.Trim().Split('.')[0];
            //        shared.addAmt = Convert.ToDecimal(shared.refundAmt);
            //        shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
            //        shareddal.updateTicketandPnr(shared);
            //        shareddal.insertLedgerDetails(shared, "Add");
            //        resp = new string[4];
            //        resp[0] = "suceess";
            //        resp[1] = objadv1.cancellationFee;
            //        resp[2] = objadv1.netRefundAmount;
            //        resp[3] = shared.tin + " Book Refrance Number :" + shared.orderID;
            //    }
            //    else
            //    {
            //        resp[0] = "Fail";
            //    }

            //}
            if (list[0].provider_name == "RB")
            {
                foreach (RepeaterItem item in rep_paxCan.Items)
                {
                    CheckBox chkk = (CheckBox)item.FindControl("chkChild");
                    if (chkk.Checked == true)
                    {
                        Label lblnetfare = (Label)item.FindControl("lblnetfare");
                        Label lblseat = (Label)item.FindControl("lblseat");
                        Label lblfare = (Label)item.FindControl("lblfare");
                        netfare += lblnetfare.Text.Trim() + ",";
                        seat += lblseat.Text.Trim() + ",";
                        fare += lblfare.Text.Trim() + ",";
                    }

                }
                for (int i = 0; i <= list.Count - 1; i++)
                {
                    originalfare += list[i].originalfare + ",";
                }
                netfare = netfare.Remove(netfare.LastIndexOf(","));
                seat = seat.Remove(seat.LastIndexOf(","));
                fare = fare.Remove(fare.LastIndexOf(","));
                originalfare = originalfare.Remove(originalfare.LastIndexOf(","));
                shared.orderID = list[0].orderID.Trim();
                shared.tin = list[0].tin.Trim();
                shared.journeyDate = list[0].journeyDate.Trim();
                shared.canPolicy = list[0].canPolicy;
                shared.canTime = list[0].canTime;
                shared.agentID = list[0].agentID;
                shared.AgencyName = list[0].AgencyName;
                shared.provider_name = list[0].provider_name;
                shared.Passengername = list[0].Passengername;
                shared.gender = list[0].gender;
                shared.netfare = netfare.Trim();
                shared.seat = seat.Trim();
                shared.fare = originalfare;
                shared.tripId = list[0].tripId;
                shared.paymentmode = list[0].paymentmode;

                //---------------call cancel request insert ---------------- 
                String[] seatarr = shared.seat.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                int seatcount = seatarr.Count();
                for (int i = 0; i < seatcount; i++)
                {
                    shared.seat=seatarr[i];
                    BUSCancellationDetails(Convert.ToString(shared.orderID), Convert.ToString(shared.seat), Convert.ToString(shared.Passengername), Convert.ToString(shared.gender), Convert.ToString(shared.agentID), Convert.ToString(shared.tripId), Convert.ToString(shared.paymentmode));
                }
      
                resp = sharedbal.cancelTicket(shared);

            }
            if (resp[0].Trim().ToUpper() == "FAIL")
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('Failed');", true);
            }
            else
            {
                if (resp[0].Trim().ToUpper() != "FAIL")
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "func", "close();", true);
                    string mytable = "<table cellpadding='0' cellspacing='4' border='0' align='center' class='divcan'>";
                    mytable += "<tr>";
                    mytable += "<td>Cancellation Charge:</td>";
                    mytable += "<td>" + resp[1].Trim() + "</td>";
                    mytable += "<td>Refunded Amount:</td>";
                    mytable += "<td>" + resp[2].Trim() + "</td>";
                    mytable += "<td>Status:</td>";
                    mytable += "<td>Cancelled against Ticket no :" + resp[3].Trim() + "</td>";
                    mytable += "</tr>";
                    mytable += "</table>";
                    divcan_Pax.Visible = false;
                    divcan.Visible = false;
                    divrefund.InnerHtml = mytable;
                }
            }
        }
        catch (Exception ex)
        {
            string str = ex.Message;
        }
    }
    protected void btnChkType_Click(object sender, EventArgs e)
    {
        GsrtcService objGsrtc = new GsrtcService();
        string HiddenValue = HiddenField1.Value;
        List<BS_SHARED.SHARED> list = (List<BS_SHARED.SHARED>)Session["list"];       
        List<BS_SHARED.SHARED> getlistGS = new List<BS_SHARED.SHARED>();
        btncancel.Visible = true;
        btnChkType.Visible = false;
        divcan_Pax.Visible = true;
        divcan.Visible = true;
        try
        {
            //getlistGS = objGsrtc.getFullCancellationDetails(list,HiddenValue);
            Session["getlistGS"] = getlistGS;

            if (getlistGS[0].status == "Fail")
            {
                divcan_Pax.InnerHtml = "<div align='center' style='color:#203240;font-size:14px;'>The Ticket Has Already been Cancelled</div>";
                btncancel.Visible = false;
            }
            else
            {
                divcan_Pax.InnerHtml = getlistGS[0].bookres;
                btncancel.Visible = true;
            }
        }
        catch (Exception ex)
        {
        }
    }

  public int BUSCancellationDetails(string orderid, string seatno, string paxname, string gender, string agentid ,string tripid,string paymentmode)
 
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        int i = 0;

        Random ss = new Random();
        int rfndn = ss.Next(87756556);
        string rmdno = "BRFND" + rfndn.ToString();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BUS_CANCELLATION_DETAILS";
                sqlcmd.Parameters.AddWithValue("@cmd", "insert");
                sqlcmd.Parameters.AddWithValue("@orderid", orderid);
                sqlcmd.Parameters.AddWithValue("@trip", tripid);
                sqlcmd.Parameters.AddWithValue("@seatno", seatno);
                sqlcmd.Parameters.AddWithValue("@paxname", paxname);
                sqlcmd.Parameters.AddWithValue("@gender", "");
                sqlcmd.Parameters.AddWithValue("@agentid", agentid);
                sqlcmd.Parameters.AddWithValue("@cancellationid", rmdno);
                sqlcmd.Parameters.AddWithValue("@refundstatus", "Requested");
                sqlcmd.Parameters.AddWithValue("@apirefundstatus", "");
                sqlcmd.Parameters.AddWithValue("@apicancellationamt",0);
                sqlcmd.Parameters.AddWithValue("@refundservicechrg", 0);
                sqlcmd.Parameters.AddWithValue("@acceptedby", "");
                sqlcmd.Parameters.AddWithValue("@accepteddate", "");
                sqlcmd.Parameters.AddWithValue("@refundedby", "");
                sqlcmd.Parameters.AddWithValue("@refundeddate", "");
                sqlcmd.Parameters.AddWithValue("@userrefundamt", 0);
                sqlcmd.Parameters.AddWithValue("@paymentmode", paymentmode);
                sqlcmd.Parameters.AddWithValue("@apicancellationstatus", "");
                i = (Int16)sqlcmd.ExecuteScalar();
                con.Close();


            }
        }
        catch (Exception ex)
        {
        }
        return i;
    }
}
